package com.trains.booking.datatypes;

public enum NumberOfTickets {
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5);

    private final int quantity;

    NumberOfTickets(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }
}

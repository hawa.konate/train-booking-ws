package com.trains.booking.datatypes;

public enum TravelClass {
    FIRST("First"),
    PREMIUM("Premium"),
    STANDARD("Standard");

    private final String description;

    TravelClass(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

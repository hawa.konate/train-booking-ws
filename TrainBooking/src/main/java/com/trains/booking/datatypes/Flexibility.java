package com.trains.booking.datatypes;

public enum Flexibility {
    FLEXIBLE(true),
    NON_FLEXIBLE(false);

    private final boolean value;

    Flexibility(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }
}

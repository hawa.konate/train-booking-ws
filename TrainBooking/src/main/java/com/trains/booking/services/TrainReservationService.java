package com.trains.booking.services;

import java.io.IOException;
import java.io.StringWriter;

import org.restlet.data.Form;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import com.trains.booking.databases.ReservationDatabase;


public class TrainReservationService {

    private StringWriter stringWriter = new StringWriter();
    private String responseStr = "";

    public TrainReservationService() {
    }

    public String bookATrain(String trainID, String numberOfSeats, String travelClass) {
    	if(AuthService.isLoggedIn()) {
    		ClientResource resource = new ClientResource("http://localhost:8181/bookTrain");
    		Form form = new Form();
    		form.add("numOfSeat", numberOfSeats);
    		form.add("trainId", trainID);
    		form.add("travelClass", travelClass);
    		
    		try {
    			resource.post(form).write(stringWriter);
    			boolean response = ReservationDatabase.saveReservation(trainID, AuthService.getUserId());
    			if(response) {
    				responseStr =  stringWriter.toString();
    			}
    		} catch (ResourceException e) {
    			e.printStackTrace();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    		return responseStr;
    	} else {
    		return "please go to the authService to register first";
    	}
	}


}

package com.trains.booking.services;

import com.trains.booking.databases.AuthDatabase;

public class AuthService {

    private static boolean isLoggedIn = false;
    private static String userId = "";
    private AuthDatabase database;
    
    public AuthService() {
    	this.database = new AuthDatabase();
    }

    public String login(String username, String password) {
    	if (database.validateUser(username, password)) {
    		isLoggedIn = true;
    		userId = username;
            return "successfully logged in";
        }
        return "unable to login, please register";
    }

    public String register(String username, String password) {
    	boolean hasregistered = database.addUser(username, password);
    	if(hasregistered) {
    		userId = username;
    		return "successfully registered";
    	} else {
    		return "unable to register, please try again";
    	}
    }
    
    public static String getUserId() {
    	return userId;
    }
    

    public static boolean isLoggedIn() {
    	return isLoggedIn;
    }

}
package com.trains.booking.services;
import java.io.IOException;

import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;


public class TrainSearchingService {
	public TrainSearchingService() {}

	public String searchByStations(String departureStation, String arrivalStation) {
		String uriAppend = "/" + departureStation + "/" + arrivalStation;
		ClientResource resource = new ClientResource("http://localhost:8181/trains"+uriAppend);
		String content = "unable to make the request";
		
		Representation representation = new StringRepresentation("");

		try {
			representation = resource.get();
			content = representation.getText();;
		} catch (ResourceException | IOException e ){
			e.printStackTrace();
		} finally {
			representation.release();
		}
		return content;

	}
		

	public String searchSpecificTrain(String departDate, String departureTime, String arrivalTime, String departureStation, String arrivalStation, String numOfSeats, String travelClass) {
		String uriAppend = "/" + departureStation + "/" + arrivalStation + "/" + departDate + "/" + departureTime 
				+ "/" + arrivalTime + "/" + numOfSeats + "/" + travelClass;
		
		ClientResource resource = new ClientResource("http://localhost:8181/trains"+uriAppend);
		Representation representation = new StringRepresentation("");
		String content = "unable to make the request";

		try {
			representation = resource.get();
			content = representation.getText();;
		} catch (ResourceException | IOException e){
			e.printStackTrace();
		} finally {
			representation.release();
		}
		return content;

	}

	
}



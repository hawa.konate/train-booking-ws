package com.trains.booking.databases;


import java.util.HashMap;
import java.util.Map;

public class ReservationDatabase {

    public final static Map<String, String> reservations = new HashMap<>();


    public boolean containsReservation(String reservationId) {
        return reservations.containsKey(reservationId);
    }


	public String getReservation(String reservationId) {
        return reservations.get(reservationId);
    }
	
	public static boolean saveReservation(String userId, String reservationID) {
		reservations.put(userId, reservationID);
		return true;
	}
	
	 public String saveToCSV(String filename) {
	    	String result = SaveAndParse.saveToCSV(reservations, filename);
	    	return result;
	    }

	    public String parseFromCSV(String filename) {
	    	String result = SaveAndParse.parseFromCSV(reservations, filename);
	    	return result;
	    }

}

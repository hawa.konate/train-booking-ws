package com.trains.booking.databases;

import java.util.HashMap;
import java.util.Map;


public class AuthDatabase {

    protected final static Map<String, String> users = new HashMap<String, String>() {{
    	put("joe", "password1");
        put("john", "password2");
        put("jack", "password3");
    }};
    

    public boolean containsUser(String username) {
        return users.containsKey(username);
    }

    public boolean validateUser(String username, String password) {
        if (containsUser(username)) {
            return users.get(username).equals(password);
        }
        return false;
    }

    public boolean addUser(String username, String password) {
        if (!containsUser(username)) {
            users.put(username, password);
            return true;
        }
        return false;
    }

    public boolean removeUser(String username) {
        if (containsUser(username)) {
            users.remove(username);
            return true;
        }
        return false;
    }

    public String saveToCSV(String filename) {
    	String result = SaveAndParse.saveToCSV(users, filename);
    	return result;
    }

    public String parseFromCSV(String filename) {
    	String result = SaveAndParse.parseFromCSV(users, filename);
    	return result;
    }


}

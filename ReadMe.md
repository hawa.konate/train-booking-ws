# README

Projet systèmes web -- Hawa KONATE/Nina ABITTAN-TENENBAUM_


### Comment lancer notre projet :

**Étapes nécessaires pour lancer le projet :**
cloner le repository
Lancer le serveur en compilant et run le fichier RestDistributor et votre serveur REST est en place !

Une fois le serveur en place il ne reste plus qu’à envoyer les requêtes. 
Ce serveur accepte 2 types de requêtes. 
Les requêtes GET permettent de filtrer des trains en fonction des paramètres:
+ Station de départ
+ Station d’arrivée
+ Heure de départ (facultatif): departureTime
+ Heure d’arrivée (facultatif):  arrivalTime
+ Nombre de siège min (facultatif): seats
+ Classe des sièges (facultatif): class
+ Jour de départ: (facultatif): day

Les requêtes POST permettent d’envoyer la commande de billet de train à la base de données et ainsi mettre à jour le nombre de sièges accessibles selon ce que souhaite acheter l’utilisateur.
Elle sont appelées avec un formulaire umx de format:
{
numOfSeat : int
trainId: int
travelClass: String
}


Pour lancer une requête, plusieures plateformes sont possibles :
+ Page web: permet de lancer des requêtes GET. Ouvrez une fenêtre web et entrer l’uri de votre choix en fonction des critères que vous voulez filtrer. Le port d’écoute est 8181. Exemple: https/localhost:8181/trains/StationA/StationB?seats=15

+ Postman: Postman est outil qui permet entre autres de tester les API. Vous pouvez lancer vos requêtes GET et POST dessus. 
Pour lancer une requête GET, créez une nouvelle requête, qui normalement devrait automatiquement être de type GET. (Si non modifier son type dans le drowpdown à gauche de l'URL). Cela étant fait, il vous suffit d’entrer votre URL avec les paramètres décrit ci dessus et recevoir votre résultat. Ex:https/localhost:8181/trains/StationA/StationB?seats=15. 
Pour effectuer une requête POST, il y a une rapide configuration à effectuer. Créez une nouvelle requête et changez son type à POST, ensuite allez dans l'onglet body et sélectionnez le format de donnée x-www-form-urlencoded. Écrivez dans l’onglet Body le corps de votre requête dans le format montré ci-dessus. Dans la barre d’URL écrivez : https/localhost:8181/bookTrain et envoyez !




## Plus d’information sur notre API

### La base de données

Notre base de données est actuellement composée de 3 classes. 

+ La table Train :
   trainID serial
   departure_station VARCHAR(50)
   arrival_station VARCHAR(50)
   departure_date DATE
   arrival_date DATE
   departure_time TIME
   arrival_time TIME
   
+ La table Seat:
    seatID SERIAL
    trainID INT NOT NULL
    pricingID INT NOT NULL
    class VARCHAR(20)
    availableSeats INT

+ La charte de prix SeatPricing:
    pricingID SERIAL,
    trainID INT NOT NULL,
    class VARCHAR(20),
    nonFlexiblePrice INT,
    flexiblePrice INT,

### Le serveur REST

Le serveur Rest récupère les requêtes HTML qui lui sont adressé et réagit en fonction avec des actions demandées et définies pour l'API. 

A la récupération d'une requête GET, notre API REST récupère les paramètres obligatoires et facultatif, vérifie leur validité et lance une recherche dans la base de donnée selon les critère données La fonction renvoie alors les données récupérées en string.

A la réception d'une requête POST, notre API récupère un formulaire contenant les paramètres qui l'intéressent. Elle va alors tenter de baisser le nombre de siège du train demandé en fonction du nombre et de la classe des siège demandés par un utilisateur. Il renvoie alors soit que les sièges ont bien été modifiés, soit il renvoit une exception NoUpdateException si ce n'est pas le cas.


### Le Service SOAP

Le service SOAP permet aux utilisateurs rechercher les trains disponibles à partir d'une station de départ et d'arriver (à minima) et de s'enregistrer/se connecter afin de réserver un train.
Ce sont les services TrainSearchingService, AuthService et TrainReservationService qui permettent de faire cela respectivement.

Les bases de données utilisées pour ces services sont des fichiers csv, enregistrés dans le dossier 'db' et générés grâce au service SaveAndParse utilisé dans AuthDatabase et ReservationDatabase.

Pour consommer l'API REST, le service SOAP créé des 'ClientResource' dans les classes TrainSearchingService et TrainReservationService pour utiliser les requêtes définies dans cette API.

Exemple (TrainReservationService) : 
```java
ClientResource resource = new ClientResource("http://localhost:8181/bookTrain");
    		Form form = new Form();
    		form.add("numOfSeat", numberOfSeats);
    		form.add("trainId", trainID);
    		form.add("travelClass", travelClass);
```
ou encore (TrainSearchingService) :
```java
public String searchByStations(String departureStation, String arrivalStation) {
		String uriAppend = "/" + departureStation + "/" + arrivalStation;
		ClientResource resource = new ClientResource("http://localhost:8181/trains"+uriAppend);
		String content = "unable to make the request";
		
		Representation representation = new StringRepresentation("");

		try {
			representation = resource.get();
			content = representation.getText();;
		} catch (ResourceException | IOException e ){
			e.printStackTrace();
		} finally {
			representation.release();
		}
		return content;

	}
```
Le service AuthService est entièrement géré par le service SOAP.

#### Voici les captures du service SOAP renvoyant les requêtes du clientResource :

<img src="login.png" alt="Service d'authentification">
<img src="searching.png" alt="Service de recherche appelant le clientResource">
<img src="reservation.png" alt="Service de réservation appelant le clientResource">

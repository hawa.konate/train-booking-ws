package filter;
 
import org.restlet.Component;
import org.restlet.data.Protocol;

public class RESTDistributor {
 
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		Component component = new Component();  
		component.getServers().add(Protocol.HTTP, 8181);

		component.getDefaultHost().attach(new RouterApplication());
		component.start();  
	}	 
 
}
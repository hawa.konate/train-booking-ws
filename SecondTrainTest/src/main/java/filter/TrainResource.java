package filter;

import java.sql.*;
import java.util.ArrayList;

import databaseconn.DBManager;
import databaseconn.NoUpdateException;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;


@ApiResponses
public class TrainResource extends ServerResource {

	private static DBManager databaseManager;
	private int idTrain;
	private String departureStation;
	private String arrivalStation;
	private String departDate;
	private String arrivalDate;
	private String departureTime;
	private String arrivalTime;



	public TrainResource(int idTrain, String departDate, String arrivalDate, String departureTime, String arrivalTime, String departureStation, String arrivalStation) {
		this.databaseManager = new DBManager();
		this.idTrain = idTrain;
		this.departDate = departDate;
		this.arrivalDate = arrivalDate;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.departureStation = departureStation;
		this.arrivalStation = arrivalStation;
	}

	public TrainResource() {
		this.databaseManager = new DBManager();
		this.idTrain = 0;
		this.departDate = "";
		this.arrivalDate = "";
		this.departureTime = "";
		this.arrivalTime = "";
		this.departureStation = "";
		this.arrivalStation = "";
	}

	@Operation(summary = "Récupérer des informations sur les trains filtrés", description = "Récupérer des informations sur les trains en fonction des critères de filtre")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Succès"),
			@ApiResponse(responseCode = "404", description = "Aucun train disponible")
	})

	@Get("txt")
	public String printFilteredTrain(
			@Parameter(name = "depart", description = "Station de départ", in = ParameterIn.PATH) String depart,
			@Parameter(name = "arrival", description = "Station d'arrivée", in = ParameterIn.PATH) String arrival,
			@Parameter(name = "departureTime", description = "Heure de départ", in = ParameterIn.QUERY) String departureTime,
			@Parameter(name = "arrivalTime", description = "Heure d'arrivée", in = ParameterIn.QUERY) String arrivalTime,
			@Parameter(name = "class", description = "Classe de voyage", in = ParameterIn.QUERY) String travelClass,
			@Parameter(name = "seats", description = "Nombre de sièges", in = ParameterIn.QUERY) String seats,
			@Parameter(name = "day", description = "Jour de départ", in = ParameterIn.QUERY) String day
	) throws Exception {
		int numberOfSeats = 1;

		String departAttribute = getAttribute("depart");
		String arrivalAttribute = getAttribute("arrival");

		String departureTimeAttribute = getQueryValue("departureTime");
		String arrivalTimeAttribute = getQueryValue("arrivalTime");
		String travelClassSeat = getQueryValue("class");


		String seatsParam = getQueryValue("seats");
		if (seatsParam != null && !seatsParam.isEmpty()) {
			try {
				numberOfSeats = Integer.parseInt(seatsParam);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		String dayAttribute = getQueryValue("day");
		if (dayAttribute == null || dayAttribute.isEmpty()) {
			dayAttribute = "2023-09-08";
		}

		ArrayList<String> trainsList = getFilteredTrains(departAttribute, arrivalAttribute, dayAttribute, departureTimeAttribute, arrivalTimeAttribute, numberOfSeats, travelClassSeat);

		if(trainsList.isEmpty())
			return "No available train";

		String response = "Information about trains leaving the " + dayAttribute + " from " + departAttribute + " to " + arrivalAttribute + ":\n\n";

		for (String train : trainsList)
			response += train + "\n";

		return response;
	}

	@Operation(summary = "Mise à jour de la disponibilité des sièges", description = "Mise à jour de la disponibilité des sièges pour un train")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Sièges mis à jour avec succès"),
			@ApiResponse(responseCode = "400", description = "Requête invalide"),
			@ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
	})
	@Post("form:txt")
	public Representation updateSeatsAvailability(@Parameter(description = "Formulaire contenant les données de mise à jour des sièges") Form form) throws SQLException {

		int numOfSeat = Integer.parseInt(form.getFirstValue("numOfSeat"));
		int trainId = Integer.parseInt(form.getFirstValue("trainId"));
		String travelClass = form.getFirstValue("travelClass");

		String updateQuery = "UPDATE Seat SET availableSeats = availableSeats - ? WHERE trainID = ? AND \"class\" = ? ";

		ArrayList<Object> paramsDeparture = new ArrayList<>();
		paramsDeparture.add(numOfSeat);
		paramsDeparture.add(trainId);
		paramsDeparture.add(travelClass);

		try {
			databaseManager.executeUpdate(updateQuery, paramsDeparture);
			return new StringRepresentation("Your reservation for the train " + trainId + " has been successfully done");
		} catch (NoUpdateException e) {
			return new StringRepresentation(e.getMessage());
		} catch (SQLException e) {
			return new StringRepresentation("Error 500: There was a problem with the database. Please check your connection and try again");
		}
	}

	public static ArrayList<String> getFilteredTrains(
			String aDepart,
			String anArrival,
			String aDay,
			String departureTime,
			String arrivalTime,
			int seats,
			String travelClass
	){
		ArrayList<String> trainsList = new ArrayList<>();
		ArrayList<Integer> trainIdList = new ArrayList<>();
		ArrayList<Object> RequestArguments = new ArrayList<>();
		RequestArguments.add(aDepart);
		RequestArguments.add(anArrival);
		RequestArguments.add(aDay);

		try {
			StringBuilder queryBuilder = new StringBuilder(
					"SELECT Train.trainID, Train.departure_station, Train.arrival_station, " +
							"Train.departure_date, Train.departure_time, " +
							"Seat.class, Seat.availableSeats, " +
							"Pricing.nonflexibleprice, Pricing.flexibleprice " +
							"FROM Train " +
							"JOIN Seat ON Train.trainID = Seat.trainID " +
							"JOIN SeatPricing AS Pricing ON Seat.pricingID = Pricing.pricingID " +
							"WHERE Train.departure_station = ? " +
							"AND Train.arrival_station = ? " +
							"AND Train.departure_date::date = CAST(? AS DATE) "
			);

			if (departureTime != null && !departureTime.isEmpty()) {
				queryBuilder.append("AND Train.departure_time = CAST(? AS TIME) ");
				RequestArguments.add(departureTime);
			}

			if (arrivalTime != null && !arrivalTime.isEmpty()) {
				queryBuilder.append("AND Train.arrival_time = CAST(? AS TIME) ");
				RequestArguments.add(arrivalTime);
			}

			if (seats > 1) {
				queryBuilder.append("AND Seat.availableSeats >= ? ");
				RequestArguments.add(seats);
			}

			if (travelClass != null && !travelClass.isEmpty()) {
				queryBuilder.append("AND Seat.class = ? ");
				RequestArguments.add(travelClass);
			}

			queryBuilder.append("ORDER BY Train.trainID;");

			ResultSet res = databaseManager.executeQuery(queryBuilder.toString(), RequestArguments);

			while (res.next()) {
				int trainID = res.getInt("trainID");
				String departureStation = res.getString("departure_station");
				String arrivalStation = res.getString("arrival_station");
				String departureDate = res.getString("departure_date");
				String departTime = res.getString("departure_time");

				String travelClassRes = res.getString("class");
				int availableSeats = res.getInt("availableSeats");
				int nonFlexiblePrice = res.getInt("nonflexibleprice");
				int flexiblePrice = res.getInt("flexibleprice");

				String trainInfo = "";

				if (!trainIdList.contains(trainID)) {
					trainIdList.add(trainID);
					trainInfo = "\n Train id: " + trainID + ", From " + departureStation + " To " + arrivalStation + ", Departure: " + departureDate + " " + departTime + "\n";
				}

				String seatInfo = "|Seat in " + travelClassRes + " class| Available Seats: " + availableSeats + ", Flexible Price: " + flexiblePrice + ", Non Flexible Price: " + nonFlexiblePrice + "\n";

				String trainDetails = trainInfo + seatInfo;
				trainsList.add(trainDetails);
			}

			if(trainsList.isEmpty())
				throw new Exception("No train found in the database");

		} catch (SQLException e) {
			System.out.println("Erreur 500: Unable to fetch data from the database");
			e.printStackTrace();
		}
		catch (Exception e) {
			System.out.println("Erreur 404: No available train");
			e.printStackTrace();
		}

		return trainsList;
	}
}
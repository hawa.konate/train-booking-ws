package filter;

import org.restlet.Application;

import org.restlet.Restlet;
import org.restlet.routing.Router;

public class RouterApplication extends Application{
	@Override
	public synchronized Restlet createInboundRoot() {
		Router router = new Router(getContext());

		router.attach("/trains/{depart}/{arrival}", TrainResource.class);
		router.attach("/bookTrain", TrainResource.class);
		return router;
	}
}
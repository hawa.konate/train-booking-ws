
import java.io.IOException;
import org.restlet.data.Form;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

public class Main {

	public static void main(String[] args){
		ClientResource resource = new ClientResource("http://localhost:8181/bookTrain");

		Form form = new Form();
		form.add("numOfSeat", "3");
		form.add("trainId", "1");
		form.add("travelClass", "First");

		try {
			resource.post(form).write(System.out);
		} catch (ResourceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

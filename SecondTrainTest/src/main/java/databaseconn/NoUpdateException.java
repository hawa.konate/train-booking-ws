package databaseconn;

import java.sql.SQLException;

public class NoUpdateException extends SQLException {
    public NoUpdateException(String message) {
        super(message);
    }
}


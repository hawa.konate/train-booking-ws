package databaseconn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DBManager {
    private Connection connection;

    public DBManager() {
        this.connection = databaseconn.ConnectDataBase.StartConnection();
    }

    public ResultSet executeQuery(String query, ArrayList<Object> params) throws SQLException {
        PreparedStatement pstmt = connection.prepareStatement(query);

        for (int i = 0; i < params.size(); i++) {
            pstmt.setObject(i + 1, params.get(i));
        }

        return pstmt.executeQuery();
    }

    public void executeUpdate(String query, ArrayList<Object> params) throws SQLException {
        PreparedStatement pstmt = connection.prepareStatement(query);

        for (int i = 0; i < params.size(); i++) {
            pstmt.setObject(i + 1, params.get(i));
        }

        int rowsAffected = pstmt.executeUpdate();
        if (rowsAffected <= 0) {
            throw new NoUpdateException("Error 400: No rows were updated, please check your request");
        }
    }

}